> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS 3781 / Advanced Database Management

## James White / jdw19g

### LIS 3781:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install something
    - Install something

2. [A2 README.md](a2/README.md "My A2 README.md file")

3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Screenshot of SQL code.
    - Screenshot of pupulated tables.
    - LIS 3781 repository link.

4. [A4 README.md](a4/README.md "My A4 README.md file")
    - LIS 3781 Repository Link.
    - A4 Business Rules
    - Screenshot of ERD.

5. [A5 README.md](a5/README.md "My A5 README.md file")
    - LIS 3781 Repository Link.
    - A5 Business Rules
    - Screenshot of ERD.

6. [P1 README.md](p1/README.md "My P1 README.md file")
    - P1 Business Rules.
    - Screenshot of ERD.
    - Screenshots of SQL Code.
    - Screenshots of SQL table inserts.
    - LIS 3781 Repository Link. 

7. [P2 README.md](p2/README.md "My P2 README.md file")
    - P2 Business Rules.
    - LIS 3781 Repository Link.

