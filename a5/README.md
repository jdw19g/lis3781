> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>
----
# LIS 3781

## jdw19g / James White

----
### Assignment 5 Requirements:

*four parts:*

1. Questions
2. SQL Code 
3. SQL Table Inserts
4. Bitbucket Repo Links:
*    a) https://bitbucket.org/jdw19g/lis3781

----
# A5 Database business rules:

* From A4:
*1. A sales representative has at least one customer, and each customer has at least one sales rep on any given day(as it is a high-volumeorganization).
*2. A customer places at least one order. However, each order is placed by only one customer.
*3. Each order contains at least one order line. Conversely, each order line is contained in exactly one order.
*4. Each product may be on a number of order lines. Though, each order line contains exactly one product id (though, each product idmay have a quantity of more than one included, e.g., “oln_qty”).
*5. Each order is billed on one invoice, and each invoice is a bill for exactly one order(by only one customer).
*6. An invoice can have one (full),or can have many payments (partial). Though, each payment is made to only one invoice.
*7. A store has many invoices, but each invoice is associated with only one store.
*8. A vendor provides many products, but each product is provided by only one vendor.
*9. Must track yearly history of sales reps, including(also,see Entity-specific attributesbelow): yearly sales goal, yearly total sales, yearly total commission (in dollars and cents).
*10. Must track history of products, including: cost, price, and discount percentage (if any).

*for A5: Add additional tables to help with the influx of business
*1. Time
*2. State
*3. Region
*4. City
*5. Store

----
#### README.md file should include the following items:

* A5 Business Rules
* Screenshot of A5 ERD


#### Assignment Screenshots:

*Screenshot of A5 ERD*:

![A5 ERD screenshot](img/A5_ERD.png)


