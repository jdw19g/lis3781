> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>
----
# LIS 3781

## jdw19g / James White

----
### Assignment 3 Requirements:

*four parts:*

1. Questions
2. SQL code 
3. SQL populated tables
4. Bitbucket Repo Links:
*    a) https://bitbucket.org/jdw19g/lis3781

----



#### README.md file should include the following items:

* Screenshot of A3 code
* Screenshot of A3 customer table
* Screenshot of A3 commodity table
* Screenshot of A3 "order" table


#### Assignment Screenshots:

*Screenshot of A3 code*:

![A3 code screenshot](img/a3_code.png)

*Screenshot of populated A3 customer table*:

![A3 populated customer screenshot](img/a3_cus_table.png)

*Screenshot of A3 populated commodity table*:

![A3 populated commodity table screenshot](img/a3_com_table.png)

*Screenshot of A3 populated "order" table*:

![A3 populated "order" table screenshot](img/a3_ord_table.png)


