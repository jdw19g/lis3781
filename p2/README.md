> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>
----
# LIS 3781

## jdw19g / James White

----
### Project 2 Requirements:

*four parts:*

1. Questions
2. MongoDB Login
3. MongoDB Questions
4. Bitbucket Repo Links:
*    a) https://bitbucket.org/jdw19g/lis3781

----
# P2 Database business rules:


*1. How to Create Database & Collection in MongoDB
*2. Add MongoDB Array using insert() with Example
*3. Mongodb Primary Key: Example to set _id field with ObjectId()
*4. MongoDB Query Document using find() with Example
*5. MongoDB Cursor Tutorial: Learn with Example
*6. MongoDB order with Sort() & Limit() Query with Examples
*7. MongoDB Count() & Remove() Functions with Examples
*8. MongoDB Update() Document with Example


----
#### README.md file should include the following items:

* P2 Business Rules


#### Assignment Screenshots:

*N/A*:




