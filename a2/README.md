> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>
----
# LIS 3781

## jdw19g / James White

----
### Assignment 2 Requirements:

*four parts:*

1. Questions
2. SQL code 
3. SQL populated tables
4. Bitbucket Repo Links:
*    a) https://bitbucket.org/jdw19g/lis3781

----
# A2 Database Table creation:

*1. **Using SQL only, NOT** MySQL Workbench:
*2. Locally: create **yourfsuid** database, and two tables: **company** and **customer**

    **note:** Also, these two tables must be populated in **yourfsuid** database on the CCI server.
 * a) use 1:M relationship: **comapny** is parent table
 * b) **company** attributes:
  *    i. cmp_id (pk)
  *   ii. cmp_type enum('C-Corp','S-Corp','Non-Profit','LLC','Partnership')
  *  iii. cmp_street
  *   iv. cmp_city
  *    v. cmp_state
  *   vi. cmp_zip (zf)
  *  vii. cmp_phone
  * viii. cmp_ytd_sales
  *   ix. cmp_url
  *    x. cmp_notes
 * c) **customer** attributes:
  *    i. cus_id (pk)
  *   ii. cmp_id (fk)
  *  iii. cus_ssn (binary 64)
  *   iv. cus_salt (binary 64)
  *    v. cus_type enum('loyal','Discount','Impulse','Need-Based','Wandering')
  *   vi. cus_first
  *  vii. cus_last
  * viii. cus_street
  *   ix. cus_city
  *    x. cus_state
  *   xi. cus_zip (zf)
  *  xii. cus_phone
  * xiii. cus_email
  *  xiv. cus_balance
  *   xv. cus_tot_sales
  *  xvi. cus_notes
 * d) Create suitable indexes **and** foreign keys
 * **e) Enforce pk/fk relationship: on update cascade, on delete restrict**


#### README.md file should include the following items:

* Screenshot of A2 code pt.1 and pt.2
* Screenshot of A2 company table
* Screenshot of A2 customer table


#### Assignment Screenshots:

*Screenshot of A2 code pt.1*:

![A2 code pt.1 screenshot](img/a2_cmp_table.png)

*Screenshot of A2 code pt.2*:

![A2 code pt.2 screenshot](img/a2_cus_table.png)

*Screenshot of populated company table*:

![Populated company table screenshot](img/a2_cmp_select.png)

*Screenshot of populated customer table*:

![Populated customer table screenshot](img/a2_cus_select.png)


