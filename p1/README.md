> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>
----
# LIS 3781

## jdw19g / James White

----
### Project 1 Requirements:

*four parts:*

1. Questions
2. SQL Code 
3. SQL Table Inserts
4. Bitbucket Repo Links:
*    a) https://bitbucket.org/jdw19g/lis3781

----
# P1 Database business rules:

*1. An attorney is retained by (or assigned to) one or more clients, for each case.
*2. A client has (or is assigned to) one or more attorneys for each case.
*3. An attorney has one or more cases.
*4. A client has one or more cases
*5. Each court has one or more judges adjudicating.
*6. Each judge adjudicates upon exactly one court.
*7. Each judge may preside over more than one case
*8. Each case that goes to court is presided over by exactly one judge.
*9. A person can have more than one phone number.

 * **NOTES:**
  * Attorney data must include social security number, name, address, office phone, home phone, e-mail,
start/end dates, dob, hourly rate, years in practice, bar (may be more than one - multivalued), specialty
(may be more than one - multivalued).
  * Client data must include social security number, name, address, phone, e-mail, dob.
  * Case data must include type, description, start/end dates.
  * Court data must include name, address, phone, e-mail, url.
  * Judge data must include same information as attorneys (except bar, specialty and hourly rate; instead,
use salary).
  * Must track judge historical data—tenure at each court (i.e., start/end dates), and salaries.
  * Also, history will track which courts judges presided over, if they took a leave of absence, or retired.
  * All tables must have notes.


----
#### README.md file should include the following items:

* P1 Business Rules
* Screenshot of P1 ERD
* Screenshot of P1 code


#### Assignment Screenshots:

*Screenshot of P1 ERD*:

![P1 ERD screenshot](img/p1_erd.png)

*Screenshot of person and attorney table attributes:

![P1 person and attorney tables screenshot](img/p1_code_1.png)

*Screenshot of client and court table attributes:

![P1 client and court tables screenshot](img/p1_code_2.png)

*Screenshot of judge and judge history table attributes:

![P1 judge and judge history tables screenshot](img/p1_code_3.png)

*Screenshot of case and bar table attributes:

![P1 case and bar tables screenshot](img/p1_code_4.png)

*Screenshot of specialty and assignment table attributes:

![P1 specialty and assignment tables screenshot](img/p1_code_5.png)

*Screenshot of phone table attributes and person table inserts:

![P1 phone table attributes and person table inserts screenshot](img/p1_code_6.png)

*Screenshot of phone, client, and attorney table inserts:

![P1 phone, client, and attorney table inserts screenshot](img/p1_code_7.png)

*Screenshot of bar, specialty, and court table inserts:

![P1 bar, specialty, and court table inserts screenshot](img/p1_code_8.png)

*Screenshot of judge, judge history, and `case` table inserts:

![P1 judge, judge history, and `case` table inserts screenshot](img/p1_code_9.png)

*Screenshot of assignment table inserts:

![P1 assignment table inserts screenshot](img/p1_code_10.png)
